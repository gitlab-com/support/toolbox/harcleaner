# Build both binaries
.PHONY: build
build:
	mkdir -p dist
	go build -o dist/harcleaner ./cmd/harcleaner
	go build -o dist/hargen ./cmd/hargen

# Run all tests
.PHONY: test
test:
	go test -v ./...

# Run benchmarks
.PHONY: benchmark
benchmark:
	go test -v "-run=^$$" -bench=. -count=10 -benchmem ./internal/harprocessor/...

# Default threshold if not set in environment
BENCHMARK_THRESHOLD ?= 5.0

# Usage: make benchmark-compare OLD=old.txt NEW=new.txt
benchmark-compare:
	@if [ -z "$(OLD)" ] || [ -z "$(NEW)" ]; then \
		echo "Usage: make benchmark-compare OLD=old.txt NEW=new.txt"; \
		exit 1; \
	fi
	./scripts/bench-compare.sh $(BENCHMARK_THRESHOLD) $(OLD) $(NEW)

# Clean build artifacts
.PHONY: clean
clean:
	rm -rf dist/
	rm -f benchmark.txt

# Install development tools
.PHONY: devtools
devtools:
	go install golang.org/x/perf/cmd/benchstat@latest

# Check code formatting without making changes
.PHONY: format-check
format-check:
	@files=$$(gofmt -l .); if [ -n "$$files" ]; then \
		echo "These files need formatting:"; \
		echo "$$files"; \
		exit 1; \
	fi

# Format code and show diffs
.PHONY: format-apply
format-apply:
	gofmt -d -w .

# Check for common coding mistakes
.PHONY: static-check
static-check:
	go vet ./...

# Run all checks
.PHONY: check
check: format-check static-check test