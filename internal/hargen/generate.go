package hargen

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"harcleaner/internal/model"
	"net/http"
	"time"
)

// Generates random alphanumeric strings
// Based on hexadecimal
func randomHex(n int) (string, error) {
	bytes := make([]byte, n)
	if _, err := rand.Read(bytes); err != nil {
		return "", err
	}
	return hex.EncodeToString(bytes), nil
}

// Generates random and junk strings that
// look like JWT tokens
func GenerateJwtString() string {
	header := []byte(`{"alg":"lol", "type":"jwt"}`)
	payload_s, _ := randomHex(10)
	payload := []byte(`{"message":` + payload_s + `}`)
	signature_s, _ := randomHex(10)
	signature := []byte(signature_s)
	jwt := base64.URLEncoding.EncodeToString(header) + "." +
		base64.URLEncoding.EncodeToString(payload) + "." +
		base64.URLEncoding.EncodeToString(signature)
	return jwt
}

// Generate random and junk strings that
// look like GitLab tokens
func GenerateGitLabToken() string {

	n := 10
	token_string, _ := randomHex(n)
	return "glpat" + "-" + token_string
}

// Wrapper for GenerateHar
// Outputs JSON instead of the HAR data structure
func GenerateHarJSON(entryCount int) []byte {
	har := GenerateHar(entryCount)

	jsonData, err := json.MarshalIndent(har, "", "    ")
	if err != nil {
		// For test utilities, it's probably okay to panic
		// if we can't generate test data
		panic(err)
	}

	return jsonData
}

// GenerateHar creates a HAR with specified number of entries
func GenerateHar(entryCount int) model.Har {
	har := model.Har{
		Log: model.Log{
			Entries: make([]model.Entry, entryCount),
		},
	}

	// Generate varied entries
	for i := 0; i < entryCount; i++ {
		har.Log.Entries[i] = generateEntry(i)
	}

	return har
}

func generateHeaders() []model.Header {
	return []model.Header{
		{Name: "Authorization", Value: "sensitive"},
		{Name: "Content-Type", Value: "application/json"},
		{Name: "x-csrf-token", Value: "sensitive"},
		{Name: "Cookie", Value: "sensitive"},
	}
}

func generateCookies() []model.Cookie {
	return []model.Cookie{
		{Name: "session", Value: "sensitive"},
		{Name: "auth", Value: "sensitive"},
	}
}

func generateQueryString(method string) []model.QueryString {
	if method != "GET" {
		return nil
	}
	return []model.QueryString{
		{Name: "token", Value: "sensitive"},
		{Name: "filter", Value: "active"},
	}
}

func generatePostData(method string) model.PostData {
	if method != "POST" && method != "PUT" {
		return model.PostData{}
	}
	return model.PostData{
		MimeType: "application/json",
		Text:     `{"password":"sensitive","data":"some content"}`,
	}
}

func generateEntry(i int) model.Entry {
	method := []string{"GET", "POST", "PUT"}[i%3]

	request := model.Request{
		Method:      method,
		Url:         fmt.Sprintf("https://example.com/api/resource/%d", i),
		Headers:     generateHeaders(),
		Cookies:     generateCookies(),
		QueryString: generateQueryString(method),
	}

	if method == "POST" || method == "PUT" {
		request.PostData = generatePostData(method)
	}

	return model.Entry{
		Request:         request,
		Response:        generateResponse(method),
		StartedDateTime: time.Now().Add(-time.Duration(i) * time.Millisecond * 100).UTC().Format(time.RFC3339),
	}
}

func generateResponse(method string) model.Response {
	status := map[string]int{
		"GET":  200,
		"POST": 201,
		"PUT":  200,
	}[method]

	return model.Response{
		Status:     status,
		StatusText: http.StatusText(status),
		Headers: []model.Header{
			{Name: "Content-Type", Value: "application/json"},
		},
		Content: model.Content{
			MimeType: "application/json",
			Text:     `{"token":"sensitive","user":{"password":"sensitive"},"status":"ok"}`,
		},
	}
}
