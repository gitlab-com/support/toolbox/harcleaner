package model

// Creating structs to define data structure of marshalled JSON
// Approximates the HAR file's data format/structure
type Har struct {
	Log Log `json:"log"`
}

type Log struct {
	Version string  `json:"version"`
	Creator Creator `json:"creator"`
	Pages   []Page  `json:"pages"`
	Entries []Entry `json:"entries"`
}

type Creator struct {
	Name    string `json:"name"`
	Version string `json:"version"`
	Comment string `json:"comment"`
}

type Page struct {
	StartedDateTime string     `json:"startedDateTime"`
	Id              string     `json:"id"`
	Title           string     `json:"title"`
	PageTimings     PageTiming `json:"pageTimings"`
}

type PageTiming struct {
	OnContentLoad float64 `json:"onContentLoad"`
	OnLoad        float64 `json:"onLoad"`
}

type Entry struct {
	Request         Request     `json:"request"`
	Response        Response    `json:"response"`
	ServerIpAddress string      `json:"serverIpAddress"`
	StartedDateTime string      `json:"startedDateTime"`
	Time            float64     `json:"time"`
	Timings         EntryTiming `json:"timings"`
}

type Request struct {
	Method      string        `json:"method"`
	Url         string        `json:"url"`
	HttpVersion string        `json:"httpVersion"`
	Headers     []Header      `json:"headers"`
	QueryString []QueryString `json:"queryString"`
	Cookies     []Cookie      `json:"cookies"`
	HeadersSize int           `json:"headersSize"`
	BodySize    int           `json:"bodySize"`
	PostData    PostData      `json:"postData"`
}

type Response struct {
	Status      int      `json:"status"`
	StatusText  string   `json:"statusText"`
	HttpVersion string   `json:"httpVersion"`
	Headers     []Header `json:"headers"`
	Cookies     []Cookie `json:"cookies"`
	Content     Content  `json:"content"`
	RedirectURL string   `json:"redirectURL"`
	HeadersSize int      `json:"headersSize"`
	BodySize    int      `json:"bodySize"`
}

type Header struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type Cookie struct {
	Name     string `json:"name"`
	Value    string `json:"value"`
	Path     string `json:"path"`
	Domain   string `json:"domain"`
	Expires  string `json:"expires"`
	HttpOnly bool   `json:"httpOnly"`
	Secure   bool   `json:"secure"`
	SameSite string `json:"sameSite"`
}

type QueryString struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type Content struct {
	Size     int    `json:"size"`
	MimeType string `json:"mimeType"`
	Text     string `json:"text"`
	Encoding string `json:"encoding"`
}

type EntryTiming struct {
	Blocked         float64 `json:"blocked"`
	Dns             float64 `json:"dns"`
	Ssl             float64 `json:"ssl"`
	Connect         float64 `json:"connect"`
	Send            float64 `json:"send"`
	Wait            float64 `json:"wait"`
	Receive         float64 `json:"receive"`
	BlockedQueueing float64 `json:"_blocked_queueing"`
}

type PostData struct {
	MimeType string          `json:"mimeType"`
	Text     string          `json:"text"`
	Params   []PostDataParam `json:"params"`
}

type PostDataParam struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}
