package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadConfig(t *testing.T) {
	tests := []struct {
		name         string
		configPath   string
		expectError  bool
		useDefault   bool
		wantSettings Settings
	}{
		{
			name:        "load default embedded config",
			configPath:  "", // Empty path to trigger default config
			expectError: false,
			useDefault:  true,
			wantSettings: Settings{
				ExactStringMatch: false, // default value
			},
		},
		{
			name:        "nonexistent config file",
			configPath:  "nonexistent.json",
			expectError: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			loader := NewConfigLoader()
			config, err := loader.LoadConfig(ConfigOptions{
				ConfigPath: tt.configPath,
			})

			if tt.expectError {
				assert.Error(t, err)
				assert.Nil(t, config)
			} else {
				assert.NoError(t, err)
				assert.NotNil(t, config)
				if tt.useDefault {
					assert.Equal(t, tt.wantSettings, config.Settings)
				}
			}
		})
	}
}
