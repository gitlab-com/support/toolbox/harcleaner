package config

import (
	"embed"
	"encoding/json"
	"fmt"
	"harcleaner/internal/logging"
	"harcleaner/internal/model"
	"os"
	"path/filepath"
)

//go:embed default_config.json
var defaultConfig embed.FS

// ConfigOptions contains options that can be passed from the command line
type ConfigOptions struct {
	ConfigPath string // Path to config file if specified via --config
}

// Config defines the structure for harcleaner configuration
type Config struct {
	Settings   Settings    `json:"settings"`
	Exceptions []Exception `json:"exceptions"`
}

type Settings struct {
	ExactStringMatch bool `json:"exactStringMatch"`
}

// Package-level settings initialized at runtime from config
var settings Settings

// Exception defines when to preserve specific parts of the HAR
type Exception struct {
	Name     string            `json:"name"`
	Criteria ExceptionCriteria `json:"criteria"`
	Actions  ExceptionActions  `json:"actions"`
}

// ExceptionCriteria maps to the existing HAR structs for matching
type ExceptionCriteria struct {
	Request  *model.Request  `json:"request,omitempty"`
	Response *model.Response `json:"response,omitempty"`
}

// ExceptionActions defines what parts of the matched entries to preserve
type ExceptionActions struct {
	PreservePostData        bool `json:"preservePostData"`
	PreservePostParams      bool `json:"preservePostParams"`
	PreserveCookies         bool `json:"preserveCookies"`
	PreserveResponseContent bool `json:"preserveResponseContent"`
}

// ConfigLoader handles loading configuration
type ConfigLoader struct {
	searchPaths []string
}

// Initialize default settings
func init() {
	settings.ExactStringMatch = false // default to partial matching
}

func GetSettings() Settings {
	return settings
}

func UpdateSettings(s Settings) {
	settings = s
}

// Function to update settings from config
func applySettings(config *Config) {
	if config.Settings != (Settings{}) {
		settings.ExactStringMatch = config.Settings.ExactStringMatch
	}
}

// NewConfigLoader creates a new config loader with standard search paths
func NewConfigLoader() *ConfigLoader {
	searchPaths := []string{
		"./harcleaner.json", // Current directory
		filepath.Join(os.Getenv("HOME"), ".config/harcleaner.json"), // User's config directory
		filepath.Join(os.Getenv("HOME"), ".harcleaner.json"),        // User's home directory
	}

	return &ConfigLoader{
		searchPaths: searchPaths,
	}
}

// LoadConfig attempts to load configuration in this order:
// 1. Config specified in ConfigOptions
// 2. User-provided config from known locations
// 3. Embedded default config
func (cl *ConfigLoader) LoadConfig(opts ConfigOptions) (*Config, error) {
	var config *Config
	var err error

	// 1. Try loading from specified config path if provided
	if opts.ConfigPath != "" {
		config, err = cl.loadUserConfig(opts.ConfigPath)
		if err != nil {
			return nil, fmt.Errorf("failed to load specified config from %s: %w", opts.ConfigPath, err)
		}
		logging.Log().Debugf("Using config from specified path: %s", opts.ConfigPath)
	}

	// 2. Try user config locations if no config yet
	if config == nil {
		for _, path := range cl.searchPaths {
			if config, err = cl.loadUserConfig(path); err == nil {
				logging.Log().Debugf("Using config found at: %s", path)
				break
			}
		}
	}

	// 3. Fall back to embedded default config if still no config
	if config == nil {
		config, err = cl.loadDefaultConfig()
		if err != nil {
			return nil, err
		}
		logging.Log().Debug("Using embedded default config")
	}

	// Apply settings before returning
	applySettings(config)
	return config, nil
}

// loadUserConfig loads configuration from a file path
func (cl *ConfigLoader) loadUserConfig(path string) (*Config, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	var config Config
	if err := json.Unmarshal(data, &config); err != nil {
		return nil, err
	}

	if err := validateConfig(&config); err != nil {
		return nil, fmt.Errorf("invalid config in %s: %w", path, err)
	}

	return &config, nil
}

// loadDefaultConfig loads the embedded default configuration
func (cl *ConfigLoader) loadDefaultConfig() (*Config, error) {
	data, err := defaultConfig.ReadFile("default_config.json")
	if err != nil {
		return nil, err
	}

	var config Config
	if err := json.Unmarshal(data, &config); err != nil {
		return nil, err
	}

	return &config, nil
}

// validateConfig ensures the configuration is valid
func validateConfig(config *Config) error {
	if config == nil {
		return fmt.Errorf("config cannot be nil")
	}

	for i, ex := range config.Exceptions {
		if ex.Name == "" {
			return fmt.Errorf("exception %d: name is required", i)
		}

		if ex.Criteria.Request == nil && ex.Criteria.Response == nil {
			return fmt.Errorf("exception %d: must specify at least one criteria", i)
		}

		// Ensure at least one action is specified
		if !(ex.Actions.PreservePostData ||
			ex.Actions.PreservePostParams ||
			ex.Actions.PreserveCookies ||
			ex.Actions.PreserveResponseContent) {
			return fmt.Errorf("exception %d: must specify at least one action", i)
		}
	}

	return nil
}

func (c *Config) String() string {
	configJson, err := json.MarshalIndent(c, "", "  ")
	if err != nil {
		return fmt.Sprintf("Error marshalling config: %v", err)
	}
	return string(configJson)
}
