package harprocessor

import (
	"encoding/json"
	"fmt"
	"harcleaner/internal/config"
	"harcleaner/internal/logging"
	"harcleaner/internal/model"
	"reflect"
	"regexp"
	"strings"
)

type RegexPatternReplace struct {
	Description     string
	Regex           string
	Replacement     string
	ContainsKeyword bool
}

type ExceptionMetric struct {
	Name    string
	Matched int
}

type Metrics struct {
	Entries              int
	Strings              int
	RegexComparisons     int
	Redactions           int
	ConfiguredExceptions int
	ExceptionMetrics     []ExceptionMetric // renamed from Exceptions for clarity
}

type Processor struct {
	har *model.Har
}

var metrics Metrics

var replace_string = "REDACTED"

var KeywordList = []string{
	"Authorization",
	"SAMLRequest",
	"SAMLResponse",
	"access_token",
	"appID",
	"assertion",
	"auth",
	"authenticity_token",
	"challenge",
	"client_id",
	"client_secret",
	"code",
	"code_challenge",
	"code_verifier",
	"email",
	"facetID",
	"fcParams",
	"id_token",
	"password",
	"refresh_token",
	"serverData",
	"shdf",
	"state",
	"token",
	"usg",
	"vses2",
	"x-client-data",
	"passwd",
	"Cookie",
}

func New(h *model.Har) *Processor {
	return &Processor{har: h}
}

// Unmarshall the data into the `Har` structure
func UnmarshallHar(b []byte) (model.Har, error) {
	var har model.Har
	err := json.Unmarshal(b, &har)
	if err != nil {
		return har, err
	}

	return har, nil
}

// Marhsall data from `Har` to JSON []byte
func MarshallHar(h model.Har) ([]byte, error) {
	b, err := json.MarshalIndent(h, "", "  ")
	if err != nil {
		return b, err
	}

	return b, nil
}

// Function to process HAR struct
func (p *Processor) Process(c *config.Config) (Metrics, error) {

	// Add number of entries to metrics
	metrics.Entries = len(p.har.Log.Entries)

	// Initialize metrics for exceptions
	if c != nil {
		metrics.ConfiguredExceptions = len(c.Exceptions)
		metrics.ExceptionMetrics = make([]ExceptionMetric, len(c.Exceptions))
		for i, ex := range c.Exceptions {
			metrics.ExceptionMetrics[i].Name = ex.Name
		}
	}

	// For each entry in the HAR file
	// Walk the Entry struct to process
	// its fields
	for i := 0; i < len(p.har.Log.Entries); i++ {
		entry := &p.har.Log.Entries[i]
		var actions *config.ExceptionActions

		// Check if any exception matches this entry
		if c != nil {
			for j := range c.Exceptions {
				if Matches(&c.Exceptions[j], entry) {
					actions = &c.Exceptions[j].Actions
					metrics.ExceptionMetrics[j].Matched++
					logging.Log().Debugf("Entry %d: Matched exception '%s' with actions: %+v",
						i, c.Exceptions[j].Name, actions)
					break
				}
			}
		}

		// Walk the Entry struct with any matched actions
		WalkEntry(entry, nil, actions)
	}

	// Add comment to HAR to identify this file was processed
	p.har.Log.Creator.Comment = "Processed by harcleaner"

	// Will implement error handling later
	return metrics, nil
}

func WalkEntry(st interface{}, parent_type reflect.Type, actions *config.ExceptionActions) {

	val := reflect.ValueOf(st)
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}

	current_type := val.Type()

	// Process whole struct
	switch val.Kind().String() {
	case "struct":
		ProcessStruct(st, actions)
	}

	// Iterate over struct fields
	for i := 0; i < val.NumField(); i++ {
		f := val.Field(i)
		switch f.Kind() {
		case reflect.Struct:
			// Walk into struct
			WalkEntry(f.Addr().Interface(), current_type, actions)
		case reflect.Slice:
			// Walk into each item in slice
			for j := 0; j < f.Len(); j++ {
				WalkEntry(f.Index(j).Addr().Interface(), current_type, actions)
			}
		case reflect.String:
			// Process each string field
			ProcessString(val.Field(i))
		}
	}
}

func ProcessStruct(st interface{}, actions *config.ExceptionActions) {

	struct_type := reflect.TypeOf(st).String()

	s := reflect.ValueOf(st).Elem()
	name := s.FieldByName("Name").String()

	// Regardless of the struct's type,
	// if the struct's Name field
	// matches any keywords, redact
	// the Value field
	match := KeywordMatch(name)
	if match {
		value := s.FieldByName("Value")
		Redact(value, "")
	}

	// Process the struct based on its type
	if strings.Contains(struct_type, "Cookie") && shouldProcess(struct_type, actions) {
		ProcessCookie(st)
	} else if strings.Contains(struct_type, "Content") && shouldProcess(struct_type, actions) {
		ProcessResponseContent(st)
	} else if strings.Contains(struct_type, "PostDataParam") && shouldProcess(struct_type, actions) {
		ProcessPostDataParam(st)
	} else if strings.Contains(struct_type, "PostData") && shouldProcess(struct_type, actions) {
		ProcessPostData(st)
	}

}

func shouldProcess(struct_type string, actions *config.ExceptionActions) bool {
	// If no actions, always process
	if actions == nil {
		return true
	}

	// Check each type and its corresponding action
	switch {
	case strings.Contains(struct_type, "Cookie"):
		return !actions.PreserveCookies
	case strings.Contains(struct_type, "Content"):
		return !actions.PreserveResponseContent
	case strings.Contains(struct_type, "PostDataParam"):
		return !actions.PreservePostParams
	case strings.Contains(struct_type, "PostData"):
		return !actions.PreservePostData
	}

	// For unknown types, process by default
	return true
}

func Redact(v reflect.Value, replace string) {
	if replace == "" {
		replace = "REDACTED"
	}

	v.SetString(replace)

	metrics.Redactions += 1

}

func KeywordMatch(s string) bool {

	keywords := strings.Join(KeywordList, "|")
	re := regexp.MustCompile(`(?i)(` + keywords + `)`)
	match := re.MatchString(s)
	return match
}

func ProcessString(v reflect.Value) {
	result := ProcessStringRegex(v.String())

	if v.String() != result {
		Redact(v, result)
	}

	metrics.Strings += 1

}

func RegexPatternReplaceList() []RegexPatternReplace {
	patterns := []RegexPatternReplace{
		RegexPatternReplace{
			Description:     `Redact value in keyword=value`,
			Regex:           `(keyword)(=){1}(.+?)(&|\?|$)`,
			Replacement:     `${1}${2}` + replace_string + `${4}`,
			ContainsKeyword: true,
		},
		RegexPatternReplace{
			Description:     `Redact JWTs`,
			Regex:           `(ey[A-Za-z0-9-_=]+)\.(ey[A-Za-z0-9-_=]+)\.[A-Za-z0-9-_.+/=]+`,
			Replacement:     replace_string,
			ContainsKeyword: false,
		},
		RegexPatternReplace{
			Description:     `Redact GitLab Tokens`,
			Regex:           `\b(glpat|glptt|GR1348941|glrt|glft|gldt|glsoat|glcbt)-[0-9a-zA-Z_\-]{20}\b`,
			Replacement:     replace_string,
			ContainsKeyword: false,
		},
		RegexPatternReplace{
			Description:     `Redact user:pass in URL`,
			Regex:           `(:\/\/){1}([^\:]+)(\:){1}([^\@]+)(\@){1}`,
			Replacement:     `${1}` + replace_string + `${3}` + replace_string + `${5}`,
			ContainsKeyword: false,
		},
		RegexPatternReplace{
			Description:     `Redact value in "keyword":"value"`,
			Regex:           `"(keyword)"\s*:\s*"([^"]+)"`,
			Replacement:     `"${1}":"` + replace_string + `"`,
			ContainsKeyword: true,
		},
	}

	return patterns
}

func ProcessStringRegex(s string) string {

	src_string := s

	patterns := RegexPatternReplaceList()
	for _, p := range patterns {
		// Break from loop if string is already
		// either empty or fully redacted
		if src_string == "" || src_string == replace_string {
			break
		}

		regex_pattern := p.Regex

		// Only process keywords if the pattern supports it
		if p.ContainsKeyword {
			// Join all keywords in a regex "OR"
			keywords_or := strings.Join(KeywordList[:], "|")
			regex_pattern = strings.ReplaceAll(p.Regex, "keyword", keywords_or)
		}

		re := regexp.MustCompile(regex_pattern)
		result := re.ReplaceAllString(src_string, p.Replacement)
		metrics.RegexComparisons += 1

		// Increment redactions metric if string has been altered
		if result != src_string {
			metrics.Redactions += 1
		}

		src_string = result
	}

	return src_string
}

func ProcessCookie(st interface{}) error {

	s := reflect.ValueOf(st).Elem()
	value := s.FieldByName("Value")

	// Redact all cookies regardless of value
	Redact(value, "")

	return nil
}

func ProcessResponseContent(st interface{}) error {
	s := reflect.ValueOf(st).Elem()
	value := s.FieldByName("Text")

	Redact(value, "")

	return nil
}

func ProcessPostData(st interface{}) error {

	s := reflect.ValueOf(st).Elem()
	mimetype := s.FieldByName("MimeType")
	value := s.FieldByName("Text")

	if mimetype.String() != "" {
		Redact(mimetype, "")
	}

	if value.String() != "" {
		Redact(value, "")
	}

	return nil
}

func ProcessPostDataParam(st interface{}) error {

	s := reflect.ValueOf(st).Elem()

	// Redact the value of all PostDataParam
	value := s.FieldByName("Value")
	Redact(value, "")

	return nil
}

func (m Metrics) String() string {
	jsonBytes, err := json.Marshal(m)
	if err != nil {
		return fmt.Sprintf("Error marshalling metrics: %v", err)
	}
	return string(jsonBytes)
}

func matchStructs(criteria, actual interface{}) bool {
	if criteria == nil {
		return true
	}

	// Get the actual values, dereferencing pointers if needed
	criteriaVal := reflect.ValueOf(criteria)
	actualVal := reflect.ValueOf(actual)

	// Dereference pointers
	if criteriaVal.Kind() == reflect.Ptr {
		criteriaVal = criteriaVal.Elem()
	}
	if actualVal.Kind() == reflect.Ptr {
		actualVal = actualVal.Elem()
	}

	// Iterate through all fields in criteria
	for i := 0; i < criteriaVal.NumField(); i++ {
		criteriaField := criteriaVal.Field(i)
		criteriaType := criteriaVal.Type().Field(i)

		// Skip if field is not set (zero value)
		if isZeroValue(criteriaField) {
			continue
		}

		actualField := actualVal.FieldByName(criteriaType.Name)
		if !actualField.IsValid() {
			return false
		}

		// Handle different types of fields
		switch criteriaField.Kind() {
		case reflect.String:
			if !matchString(criteriaField.String(), actualField.String()) {
				return false
			}
		case reflect.Int:
			if criteriaField.Int() != actualField.Int() {
				return false
			}
		case reflect.Slice:
			if !matchSlice(criteriaField, actualField) {
				return false
			}
		case reflect.Struct:
			if !matchStructs(criteriaField.Addr().Interface(), actualField.Addr().Interface()) {
				return false
			}
		case reflect.Ptr:
			if criteriaField.IsNil() {
				continue
			}
			if actualField.IsNil() {
				return false
			}
			if !matchStructs(criteriaField.Interface(), actualField.Interface()) {
				return false
			}
		}
	}

	return true
}

func matchString(criteria, actual string) bool {
	if config.GetSettings().ExactStringMatch {
		return strings.EqualFold(criteria, actual)
	}
	return strings.Contains(strings.ToLower(actual), strings.ToLower(criteria))
}

func matchSlice(criteria, actual reflect.Value) bool {
	// If criteria slice is empty, consider it a match
	if criteria.Len() == 0 {
		return true
	}

	// For slices like Headers, match if all criteria items are found
	for i := 0; i < criteria.Len(); i++ {
		criteriaItem := criteria.Index(i)
		found := false

		for j := 0; j < actual.Len(); j++ {
			actualItem := actual.Index(j)
			if matchStructs(criteriaItem.Addr().Interface(), actualItem.Addr().Interface()) {
				found = true
				break
			}
		}

		if !found {
			return false
		}
	}

	return true
}

func isZeroValue(v reflect.Value) bool {
	switch v.Kind() {
	case reflect.String:
		return v.String() == ""
	case reflect.Int:
		return v.Int() == 0
	case reflect.Slice:
		return v.Len() == 0
	case reflect.Struct:
		return reflect.DeepEqual(v.Interface(), reflect.Zero(v.Type()).Interface())
	case reflect.Ptr:
		return v.IsNil()
	default:
		return false
	}
}

// Matches checks if an Entry matches the exception's criteria
func Matches(ex *config.Exception, entry *model.Entry) bool {
	// Check request criteria if specified
	if ex.Criteria.Request != nil {
		if !matchStructs(ex.Criteria.Request, &entry.Request) {
			return false
		}
	}

	// Check response criteria if specified
	if ex.Criteria.Response != nil {
		if !matchStructs(ex.Criteria.Response, &entry.Response) {
			return false
		}
	}

	return true
}
