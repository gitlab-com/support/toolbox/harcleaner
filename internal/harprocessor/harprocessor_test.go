package harprocessor

import (
	"fmt"
	"harcleaner/internal/config"
	"harcleaner/internal/hargen"
	"harcleaner/internal/model"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	MimeTypeJSON = "application/json"
	MimeTypeHTML = "text/html"
	MimeTypeForm = "application/x-www-form-urlencoded"
	// Add others as needed
)

const (
	PathGraphQL = "/api/graphql"
	PathAPIv4   = "/api/v4/"
)

var sensitiveValue = "sensitive"

var jsonString = fmt.Sprintf(`{"token":"%s","user":{"password":"%s"},"status":"ok"}`, sensitiveValue, sensitiveValue)
var graphqlString = `{"query": "query { getProjects { name } }"}`

func newJSONContent() model.Content {
	return model.Content{
		Text:     jsonString,
		MimeType: MimeTypeJSON,
	}
}

func newJSONPostData() model.PostData {
	return model.PostData{
		MimeType: MimeTypeJSON,
		Text:     jsonString,
		Params: []model.PostDataParam{
			{
				Name:  "param1",
				Value: sensitiveValue,
			},
		},
	}
}

func newHTMLContent() model.Content {
	return model.Content{
		Text:     fmt.Sprintf("<html>%s</html>", sensitiveValue),
		MimeType: MimeTypeHTML,
	}
}

func newAPIException() config.Exception {
	return config.Exception{
		Name: "Preserve API content",
		Criteria: config.ExceptionCriteria{
			Request: &model.Request{
				Url: PathAPIv4,
			},
		},
		Actions: config.ExceptionActions{
			PreserveResponseContent: true,
		},
	}
}

func newGraphqlException() config.Exception {
	return config.Exception{
		Name: "GraphQL API Calls",
		Criteria: config.ExceptionCriteria{
			Request: &model.Request{
				Method: "POST",
				Url:    PathGraphQL,
				PostData: model.PostData{
					MimeType: MimeTypeJSON,
				},
			},
		},
		Actions: config.ExceptionActions{
			PreservePostData: true,
		},
	}
}

// Helper to create a single header for testing
func newHeader(name, value string) model.Header {
	return model.Header{
		Name:  name,
		Value: value,
	}
}

func newHeaders(headers ...model.Header) []model.Header {
	return headers
}

func newCommonHeaders() []model.Header {
	return []model.Header{
		{Name: "Auth", Value: sensitiveValue},
		{Name: "Content-Type", Value: MimeTypeJSON},
		{Name: "x-csrf-token", Value: sensitiveValue},
		{Name: "Cookie", Value: sensitiveValue},
	}
}

func newCommonCookies() []model.Cookie {
	return []model.Cookie{
		{Name: "session", Value: sensitiveValue},
		{Name: "auth", Value: sensitiveValue},
	}
}

func TestProcessPostDataParam(t *testing.T) {
	mock := newJSONPostData().Params[0]

	ProcessPostDataParam(&mock)

	if mock.Value != replace_string {
		t.Errorf("Expected Value to be redacted")
	}
}

func TestProcessPostData(t *testing.T) {
	mock := newJSONPostData()

	ProcessPostData(&mock)

	if mock.MimeType != replace_string {
		t.Errorf("Expected MimeType to be redacted")
	}

	if mock.Text != replace_string {
		t.Errorf("Expected Text to be redacted")
	}
}

func TestProcessResponseContent(t *testing.T) {
	content := newHTMLContent()

	err := ProcessResponseContent(&content)
	if err != nil {
		t.Errorf("ProcessResponseContent() error = %v", err)
	}

	if content.Text != replace_string {
		t.Error("ProcessResponseContent() should always redact content")
	}
}

func TestContentRedactionWithExceptions(t *testing.T) {
	tests := []struct {
		name         string
		requestURL   string
		wantRedacted bool
		description  string
	}{
		{
			name:         "preserves content when URL matches",
			requestURL:   PathAPIv4 + "users",
			wantRedacted: false,
			description:  "Content should be preserved when URL matches pattern",
		},
		{
			name:         "redacts content when URL doesn't match",
			requestURL:   "/different/path",
			wantRedacted: true,
			description:  "Content should be redacted when URL doesn't match pattern",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			entry := model.Entry{
				Request: model.Request{
					Url: tt.requestURL,
				},
				Response: model.Response{
					Content: newJSONContent(),
				},
			}

			cfg := &config.Config{
				Exceptions: []config.Exception{newAPIException()},
			}

			processor := New(&model.Har{
				Log: model.Log{
					Entries: []model.Entry{entry},
				},
			})

			_, err := processor.Process(cfg)
			if err != nil {
				t.Errorf("Process() error = %v", err)
			}

			wasRedacted := processor.har.Log.Entries[0].Response.Content.Text == replace_string
			if wasRedacted != tt.wantRedacted {
				t.Errorf("%s: content redaction = %v, want %v\n  Description: %s",
					tt.name, wasRedacted, tt.wantRedacted, tt.description)
			}
		})
	}
}

func TestProcessCookie(t *testing.T) {
	mock := newCommonCookies()[0]

	ProcessCookie(&mock)

	if mock.Value != replace_string {
		t.Errorf("Expected cookie value to be redacted")
	}
}

func TestProcessStringRegex(t *testing.T) {
	tests := []struct {
		name        string
		input       string
		want        string
		description string
	}{
		{
			name:        "redacts keyword=value pattern",
			input:       fmt.Sprintf("token=%s&code=%s&other=value", sensitiveValue, sensitiveValue),
			want:        fmt.Sprintf("token=%s&code=%s&other=value", replace_string, replace_string),
			description: "Should redact sensitive values in key=value pairs",
		},
		{
			name:        "redacts JWT token",
			input:       hargen.GenerateJwtString(),
			want:        replace_string,
			description: "Should redact JWT format tokens",
		},
		{
			name:        "redacts GitLab token",
			input:       hargen.GenerateGitLabToken(),
			want:        replace_string,
			description: "Should redact GitLab personal access tokens",
		},
		{
			name:        "redacts user:pass in URL",
			input:       fmt.Sprintf("https://%s:%s@gitlab.com", sensitiveValue, sensitiveValue),
			want:        fmt.Sprintf("https://%s:%s@gitlab.com", replace_string, replace_string),
			description: "Should redact credentials in URLs",
		},
		{
			name:        `redacts "keyword":"value" pattern`,
			input:       fmt.Sprintf(`{"token":"%s","password":"%s"}`, sensitiveValue, sensitiveValue),
			want:        fmt.Sprintf(`{"token":"%s","password":"%s"}`, replace_string, replace_string),
			description: "Should redact values in JSON-style key:value pairs",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := ProcessStringRegex(tt.input)

			if got != tt.want {
				t.Errorf("ProcessStringRegex() = %v, want %v", got, tt.want)
			}

			if strings.Contains(got, sensitiveValue) {
				t.Error("Output contains unredacted sensitive value")
			}
		})
	}
}

func TestWalkEntry(t *testing.T) {
	mock := model.Entry{
		Request: model.Request{
			Headers: newCommonHeaders(),
		},
		Response: model.Response{
			Content: newHTMLContent(),
		},
	}

	WalkEntry(&mock, nil, nil)

	if mock.Request.Headers[0].Value != replace_string {
		t.Errorf("Expected header value to be redacted")
	}

	if mock.Response.Content.Text != replace_string {
		t.Errorf("Expected content text to be redacted")
	}
}

func TestMarshallHar(t *testing.T) {

	mock := model.Har{
		Log: model.Log{
			Entries: []model.Entry{
				{Request: model.Request{Method: "GET"}},
			},
		},
	}

	json, err := MarshallHar(mock)
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(json), `"method": "GET"`) {
		t.Errorf("Expected JSON to contain GET method")
	}

}

func TestUnmarshallHar(t *testing.T) {
	// Simple HAR JSON with minimal data needed
	jsonData := []byte(`{
        "log": {
            "entries": [
                {"request": {"method": "GET"}},
                {"request": {"method": "POST"}}
            ]
        }
    }`)

	har, err := UnmarshallHar(jsonData)
	if err != nil {
		t.Fatal(err)
	}

	if len(har.Log.Entries) != 2 {
		t.Errorf("Expected 2 entries, got %d", len(har.Log.Entries))
	}
}

func TestProcess(t *testing.T) {

	mock := model.Har{
		Log: model.Log{
			Entries: []model.Entry{
				{
					Request: model.Request{
						Headers: newCommonHeaders(),
					},
				},
			},
		},
	}

	processor := New(&mock)
	metrics, err := processor.Process(nil)
	if err != nil {
		t.Fatal(err)
	}

	// Check redaction occurred
	got := mock.Log.Entries[0].Request.Headers[0].Value
	want := replace_string
	if got != want {
		t.Errorf("header value = %q, want %q", got, want)
	}

	// Check metrics were recorded
	if metrics.Redactions == 0 {
		t.Error("metrics.Redactions = 0, want > 0")
	}

}

func TestShouldProcess(t *testing.T) {
	tests := []struct {
		name       string
		structType string
		actions    *config.ExceptionActions
		shouldProc bool
	}{
		{
			name:       "nil actions should always process",
			structType: "PostData",
			actions:    nil,
			shouldProc: true,
		},
		{
			name:       "should not process preserved PostData",
			structType: "PostData",
			actions:    &config.ExceptionActions{PreservePostData: true},
			shouldProc: false,
		},
		{
			name:       "should process non-preserved PostData",
			structType: "PostData",
			actions:    &config.ExceptionActions{PreservePostData: false},
			shouldProc: true,
		},
		// Add similar tests for Cookie, Content, PostDataParam
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := shouldProcess(tt.structType, tt.actions)
			assert.Equal(t, tt.shouldProc, result)
		})
	}
}

func TestProcessWithExceptions(t *testing.T) {
	// Create a test HAR with a GraphQL request
	input := model.Har{
		Log: model.Log{
			Entries: []model.Entry{
				{
					Request: model.Request{
						Method: "POST",
						Url:    PathGraphQL,
						PostData: model.PostData{
							MimeType: MimeTypeJSON,
							Text:     graphqlString,
						},
					},
				},
			},
		},
	}

	cfg := &config.Config{
		Exceptions: []config.Exception{
			newGraphqlException(),
		},
	}

	// Simply ignore metrics if we don't need them
	processor := New(&input)
	_, err := processor.Process(cfg)
	assert.NoError(t, err)

	// Verify GraphQL query was preserved
	want := graphqlString
	got := input.Log.Entries[0].Request.PostData.Text
	assert.Equal(t, want, got, "GraphQL query should be preserved by exception")
}

func TestExceptionMatches(t *testing.T) {

	tests := []struct {
		name        string
		exception   config.Exception
		entry       model.Entry
		shouldMatch bool
		description string
	}{
		{
			name:      "matches GraphQL request",
			exception: newGraphqlException(),
			entry: model.Entry{
				Request: model.Request{
					Method:  "POST",
					Url:     PathGraphQL,
					Headers: newHeaders(newHeader(":path", PathGraphQL)),
					PostData: model.PostData{
						MimeType: MimeTypeJSON,
					},
				},
			},
			shouldMatch: true,
			description: "Should match when all GraphQL criteria are met",
		},
		{
			name:      "does not match different path",
			exception: newGraphqlException(),
			entry: model.Entry{
				Request: model.Request{
					Method:  "POST",
					Headers: newHeaders(newHeader(":path", "/api/rest")),
				},
			},
			shouldMatch: false,
			description: "Should not match when path is different",
		},
		{
			name: "matches with response criteria",
			exception: config.Exception{
				Name: "JSON Response",
				Criteria: config.ExceptionCriteria{
					Response: &model.Response{
						Status:  200,
						Content: newJSONContent(),
					},
				},
			},
			entry: model.Entry{
				Response: model.Response{
					Status:  200,
					Content: newJSONContent(),
				},
			},
			shouldMatch: true,
			description: "Should match when response criteria matches",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			matches := Matches(&tt.exception, &tt.entry)
			assert.Equal(t, tt.shouldMatch, matches, tt.description)
		})
	}
}

func TestStringMatching(t *testing.T) {
	tests := []struct {
		name      string
		settings  config.Settings
		criteria  string
		actual    string
		fieldName string
		want      bool
	}{
		{
			name:     "exact match - matches",
			settings: config.Settings{ExactStringMatch: true},
			criteria: "test",
			actual:   "TEST",
			want:     true,
		},
		{
			name:     "exact match - no partial match",
			settings: config.Settings{ExactStringMatch: true},
			criteria: "test",
			actual:   "testing",
			want:     false,
		},
		{
			name:     "partial match - contains",
			settings: config.Settings{ExactStringMatch: false},
			criteria: "test",
			actual:   "testing",
			want:     true,
		},
		{
			name:      "url partial match - google",
			settings:  config.Settings{ExactStringMatch: false},
			criteria:  "https://google.com",
			actual:    "https://google.com/search?q=test",
			fieldName: "Url",
			want:      true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Set package-level settings
			config.UpdateSettings(tt.settings)
			got := matchString(tt.criteria, tt.actual)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestExceptionMatchingWithSettings(t *testing.T) {

	tests := []struct {
		name        string
		settings    config.Settings
		exception   config.Exception
		entry       model.Entry
		want        bool
		description string
	}{
		{
			name:     "google url match with partial matching",
			settings: config.Settings{ExactStringMatch: false},
			exception: config.Exception{
				Name: "Google",
				Criteria: config.ExceptionCriteria{
					Request: &model.Request{
						Url: "https://google.com",
					},
				},
			},
			entry: model.Entry{
				Request: model.Request{
					Url: "https://google.com/search?q=test",
				},
			},
			want:        true,
			description: "Should match when URL partially matches and exact matching is off",
		},
		{
			name:     "graphql path header match",
			settings: config.Settings{ExactStringMatch: true},
			exception: config.Exception{
				Name: "GraphQL",
				Criteria: config.ExceptionCriteria{
					Request: &model.Request{
						Headers: newHeaders(newHeader(":path", PathGraphQL)),
					},
				},
			},
			entry: model.Entry{
				Request: model.Request{
					Url:     PathGraphQL,
					Headers: newHeaders(newHeader(":path", PathGraphQL)),
				},
			},
			want:        true,
			description: "Should match exact paths even with exact matching enabled",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Reset settings after test
			defer func(old config.Settings) {
				config.UpdateSettings(old)
			}(config.GetSettings())

			config.UpdateSettings(tt.settings)
			got := Matches(&tt.exception, &tt.entry)
			assert.Equal(t, tt.want, got, tt.description)
		})
	}
}
