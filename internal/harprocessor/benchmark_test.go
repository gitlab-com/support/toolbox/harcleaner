package harprocessor

import (
	"fmt"
	"harcleaner/internal/config"
	"harcleaner/internal/hargen"
	"harcleaner/internal/model"
	"testing"
)

func runProcessBenchmark(b *testing.B, size int, c *config.Config, name string) {
	b.Run(name, func(b *testing.B) {
		har := hargen.GenerateHar(size)
		processor := New(&har)

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			_, err := processor.Process(c)
			if err != nil {
				b.Fatal(err)
			}
		}
	})
}

func BenchmarkProcess(b *testing.B) {
	sizes := []int{50, 100, 200}
	for _, size := range sizes {
		runProcessBenchmark(b, size, nil, fmt.Sprintf("NoConfig-Entries-%d", size))
	}
}

func BenchmarkProcessWithExceptions(b *testing.B) {
	sizes := []int{50, 100, 200}
	config := &config.Config{
		Exceptions: []config.Exception{
			{
				Name: "API POST Requests",
				Criteria: config.ExceptionCriteria{
					Request: &model.Request{
						Method: "POST",
						Url:    "/api/",
					},
				},
				Actions: config.ExceptionActions{
					PreservePostData: true,
				},
			},
		},
	}

	for _, size := range sizes {
		runProcessBenchmark(b, size, config, fmt.Sprintf("WithConfig-Entries-%d", size))
	}
}
