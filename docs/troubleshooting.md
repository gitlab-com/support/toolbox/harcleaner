## Troubleshooting

## Debug logs
Use the `-debug` CLI flag to increase the verbosity of the logging. This should include details such as:
- The source of the configuration file used
- The configuration content loaded
- Which entries matched an exception, and what actions are being taken

## “harcleaner” cannot be opened because the developer cannot be verified

More information on this error can be found on Apple's macOS user guide:
https://support.apple.com/en-au/guide/mac-help/mh40616/mac

Follow the instructions on the user guide to grant an exception in your security settings
for the app.