# Configuration

harcleaner can be configured using a JSON configuration file. The configuration allows you to:
- Control global settings, such as string matching behavior
- Define exceptions to default redaction rules
- Specify actions for entries that match exceptions

## Configuration Loading Order
Configuration is loaded in this order of precedence. Configuration is not merged, the first matching configuration file is used:

- File specified with `-config-file` flag
- `harcleaner.json` in current directory
- `~/.config/harcleaner.json`
- `~/.harcleaner.json`
- Default embedded configuration

## File Structure

The configuration file follows this structure:
```json
{
    "settings": {
        "exactStringMatch": false
    },
    "exceptions": [
        {
            "name": "Exception Name",
            "criteria": {
                "request": {
                    // HAR request fields
                },
                "response": {
                    // HAR response fields
                }
            },
            "actions": {
                "preservePostData": true,
                "preservePostParams": false,
                "preserveCookies": false,
                "preserveResponseContent": false
            }
        }
    ]
}
```

## Settings
### String Matching

`exactStringMatch`: Controls how strings are matched

 - `false` (default): Use partial matching (contains)
 - `true`: Use exact matching


## Exceptions

### Exception Matching

Exceptions allow you to preserve specific data that would normally be redacted. Each exception defines:
- Criteria to match entries
- Actions to take when matched

### HAR Schema Alignment

Exception criteria follow the same schema as HAR files, making it intuitive to define what to match. Any field in the HAR file can be used as matching criteria.

A HAR file contains one or more `entries`, each representing a single HTTP transaction. Each entry has a specific root-level structure defined in `harprocessor/harprocessor.go`:
```go
type Entry struct {
	Request         Request     `json:"request"`
	Response        Response    `json:"response"`
    ...
}
```

Each exception matches against a complete entry. You can specify criteria for:
  - Just the request
  - Just the response
  - Both request and response (all criteria must match)

Example HAR file Entry:
```json
{
  "request": {
    "method": "POST",
    "url": "https://gitlab.com/api/graphql",
    "headers": [
      {
        "name": ":path",
        "value": "/api/graphql"
      }
    ]
  }
}
```

Corresponding Exception:
```json
{
  "criteria": {
    "request": {
      "method": "POST",
      "headers": [
        {
          "name": ":path",
          "value": "/api/graphql"
        }
      ]
    }
  }
}
```

### Matching Logic

1. Each HAR entry is checked against all configured exceptions
1. For an exception to match:

  - All specified criteria must match (AND logic)
  - Unspecified fields are ignored
  - String matching follows the global exactStringMatch setting
1. First matching exception's actions are applied
1. Keyword and regex string matches are **always** redacted regardless of configured exceptions. However, these are targeted redactions; the exceptions are for redaction rules that remove entire structures like Cookies, PostData, ResponseContent, etc.

### String Matching
When `exactStringMatch` is false (default):

  - Strings match if one contains the other
  - Case-insensitive comparison
  - Useful for URL matching

When exactStringMatch is true:

  - Strings must match exactly
  - Still case-insensitive
  - More precise control


## Example 1: Preserve cookies when the request URL is google.com
`harcleaner.json`:
```json
{
    "settings": {
        "exactStringMatch": false
    },
    "exceptions": [
        {
            "name": "When requesting google.com, preserve Cookies",
            "criteria": {
                "request": {
                    "url": "https://google.com"
                }
            },
            "actions": {
                "preserveCookies": true
            }
        }
    ]
}
```