# Docker Usage

There are two ways to run harcleaner in Docker: using volumes or copying files directly.

## Quick Start
```bash
# Pull the latest image
docker pull registry.gitlab.com/gitlab-com/support/toolbox/harcleaner:latest

# Run with a volume mount
docker run --rm -v $(pwd):/tmp/harcleaner harcleaner:latest
```

## Running Options

### Using Volumes (Recommended)

If your Docker setup supports volume mounting:

1. Create and enter a directory:

```bash
mkdir harfiles && cd harfiles
```

1. Copy your HAR file (naming it `input.har`):

```bash
cp /path/to/your/file.har input.har
```

1. Run harcleaner:

```bash
docker run \
  --name harcleaner \
  --rm \
  -v $(pwd):/tmp/harcleaner/ \
  harcleaner:latest
```

The cleaned file will be written as `clean.har` in the current directory.

### Without Volumes

Some Docker environments have limitations with volume mounting or synchronization issues. For example, certain virtualization setups may have problems keeping mounted volumes in sync. This method provides a workaround by copying files directly to and from the container.

If your environment doesn't support volume mounting:

```bash
# Set your input file
INPUT_FILE="your_file.har"

# Run container
docker create --name harcleaner harcleaner:latest
docker cp "$INPUT_FILE" harcleaner:/tmp/harcleaner/input.har
docker start -ai harcleaner
docker cp harcleaner:/tmp/harcleaner/clean.har .
docker rm harcleaner
```

## Environment Variables

- `INPUT_FILE`: Name of input file (default: "input.har")
- `OUTPUT_FILE`: Name of output file (default: "clean.har")

## Available Images

Images are available from our [Container Registry](https://gitlab.com/gitlab-com/support/toolbox/harcleaner/container_registry/8089246):

```bash
# Latest release
registry.gitlab.com/gitlab-com/support/toolbox/harcleaner:latest

# Specific version
registry.gitlab.com/gitlab-com/support/toolbox/harcleaner:v2.1.0-amd64
```