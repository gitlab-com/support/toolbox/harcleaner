# Generating HAR Files

A HAR (HTTP Archive) file captures all HTTP transactions from a browser session. When generating HAR files that may contain sensitive information, follow these practices to minimize risk.

## Best Practices

### Use Private/Incognito Mode
- Start with a fresh browser session
- No existing cookies or cached credentials
- Clean session data

### Minimize Service Access
- Log in only to required services
- Avoid accessing unrelated sites
- Capture only necessary interactions

### Generate the HAR File
1. Open browser Developer Tools
2. Select the Network tab
3. Enable "Preserve log"
4. Perform the actions you need to capture
5. Save the HAR file

### Clean Up
1. Log out of all services
2. Close the Private/Incognito window
3. Process the HAR file with harcleaner

This will ensure that session tokens/credentials can't be used, even if the sensitive data is compromised in the HAR file. Consider this a defense-in-depth approach: the logout and window closure invalidate sessions, while harcleaner removes sensitive data from the file itself.

## Browser-Specific Instructions

For detailed instructions on generating HAR files in different browsers (Chrome, Firefox, Safari, Edge), see [Okta's HAR file generation guide](https://help.okta.com/oag/en-us/content/topics/access-gateway/troubleshooting-with-har.htm).

Follow those instructions using the best practices described above to minimize sensitive data capture.