# How Redaction Works

harcleaner uses multiple strategies to protect sensitive data in HAR files:

## String Pattern Matching
Every string field in the HAR file is checked for:
- Known sensitive keywords (auth, token, password, etc.)
- Common credential formats (JWT tokens, GitLab tokens)
- Key-value pairs containing sensitive data
- Embedded credentials in URLs

## Default Structure Redaction
Certain HAR structures are redacted by default:
- Cookies
- POST data
- HTML/JavaScript response content
- Form parameters

## Configuration and Exceptions
While string pattern matching always runs for security, structure redaction can be configured:
- Exception rules can preserve specific structures that are redacted by default
- Pattern matching uses exact or partial string matching
- See [Configuration](configuration.md) for detailed options

## Example
Given this (simplified) HAR entry:
```json
{
    "request": {
        "url": "https://user:password@example.com",
        "headers": [
            {"name": "Authorization", "value": "Bearer xyz"}
        ],
        "postData": {
            "text": "{\"password\":\"secret\"}"
        }
    }
}
```

The tool will:

- Redact the URL credentials
- Redact the Authorization header
- Redact the JSON password field
- Remove the postData structure (unless preserved by config)