FROM golang:1.21.4-alpine3.18

ENV INPUT_FILE="/tmp/harcleaner/input.har"
ENV OUTPUT_FILE="/tmp/harcleaner/clean.har"

WORKDIR /harcleaner
COPY . .

RUN go build -o /usr/bin/harcleaner
RUN mkdir -p /tmp/harcleaner

CMD harcleaner -debug -input "${INPUT_FILE}" -output "${OUTPUT_FILE}"
