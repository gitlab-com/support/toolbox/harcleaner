#!/bin/bash

# Check required arguments
if [ "$#" -lt 3 ]; then
    echo "Usage: $0 <threshold> <old_benchmark> <new_benchmark>"
    echo "Example: $0 5.0 old.txt new.txt"
    exit 1
fi

threshold=$1
old_file=$2
new_file=$3

echo "Using regression threshold: ${threshold}%"
echo "Comparing benchmarks:"
echo "  Old: $old_file"
echo "  New: $new_file"

benchstat_output=$(benchstat "$old_file" "$new_file")
echo "$benchstat_output"

echo "Checking for performance regressions..."
echo "$benchstat_output" | grep -o '+[0-9]\+\.[0-9]\+%' | while read percentage; do
    # Strip + and % using sed
    value=$(echo "$percentage" | sed 's/^+\(.*\)%$/\1/')
    echo "Found change of $value%"
    
    # Compare with threshold
    if (( $(echo "$value > $threshold" | bc -l) )); then
        echo "Regression above threshold: $value%"
        exit 1
    fi
done

# Check if the while loop exited with failure
if [ ${PIPESTATUS[2]} -eq 1 ]; then
    echo "Performance regression detected! Found changes above ${threshold}%"
    exit 1
else
    echo "No significant performance regressions found"
fi