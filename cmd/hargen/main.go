package main

import (
	"flag"
	"fmt"
	"harcleaner/internal/hargen"
	"os"
)

func main() {
	entries := flag.Int("entries", 3, "Number of entries to generate")
	outFile := flag.String("out", "", "Output file (if not specified, prints to stdout)")
	flag.Parse()

	jsonData := hargen.GenerateHarJSON(*entries)

	if *outFile != "" {
		if err := os.WriteFile(*outFile, jsonData, 0644); err != nil {
			fmt.Fprintf(os.Stderr, "Error writing to file: %v\n", err)
			os.Exit(1)
		}
	} else {
		fmt.Printf("%s\n", string(jsonData))
	}
}
