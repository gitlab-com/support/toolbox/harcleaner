# This is an example .goreleaser.yml file with some sensible defaults.
# Make sure to check the documentation at https://goreleaser.com

# The lines below are called `modelines`. See `:help modeline`
# Feel free to remove those if you don't want/need to use them.
# yaml-language-server: $schema=https://goreleaser.com/static/schema.json
# vim: set ts=2 sw=2 tw=0 fo=cnqoj

version: 2

gitlab_urls:
  api: "{{ .Env.CI_API_V4_URL }}"
  download: "{{ .Env.CI_SERVER_URL }}"
  use_job_token: true

before:
  hooks:
    # You may remove this if you don't use go modules.
    - go mod tidy
    # you may remove this if you don't need go generate
    #- go generate ./...

builds:
  - id: harcleaner
    env:
      - CGO_ENABLED=0
    goos:
      - linux
      - windows
      - darwin
    main: ./cmd/harcleaner
    binary: harcleaner

  - id: hargen
    env:
      - CGO_ENABLED=0
    goos:
      - linux
      - windows
      - darwin
    main: ./cmd/hargen
    binary: hargen

archives:
  - builds: [harcleaner]
    format: tar.gz
    # this name template makes the OS and Arch compatible with the results of `uname`.
    name_template: >-
      {{ .ProjectName }}_
      {{- title .Os }}_
      {{- if eq .Arch "amd64" }}x86_64
      {{- else if eq .Arch "386" }}i386
      {{- else }}{{ .Arch }}{{ end }}
      {{- if .Arm }}v{{ .Arm }}{{ end }}
    # use zip for windows archives
    format_overrides:
      - goos: windows
        format: zip

dockers:
  - id: harcleaner
    goos: linux
    goarch: amd64
    image_templates:
      - "registry.gitlab.com/gitlab-com/support/toolbox/harcleaner:{{ .Tag }}-amd64"
      - "registry.gitlab.com/gitlab-com/support/toolbox/harcleaner:latest"
    dockerfile: Dockerfile.goreleaser

brews:
  - name: harcleaner
    homepage: "https://gitlab.com/gitlab-com/support/toolbox/harcleaner"
    description: "Command line tool to clean HAR files to reduce risk of exposure of sensitive information."
    license: "MIT"
    repository:
      owner: gitlab-com/support/toolbox
      name: homebrew-harcleaner
      token: "{{ .Env.TAP_PROJECT_TOKEN }}"
      branch: brew-releases/{{ .Version }}
      pull_request:
        enabled: true
    directory: Formula
    commit_author:
      name: "{{ .Env.GITLAB_USER_NAME }}"
      email: "{{ .Env.GITLAB_USER_EMAIL }}"
    install: |
      bin.install "harcleaner"

winget:
  - name: harcleaner
    publisher: GitLab-Support
    license: MIT
    copyright: Copyright (c) GitLab
    homepage: https://gitlab.com/gitlab-com/support/toolbox/harcleaner
    short_description: Command line tool to clean HAR files to reduce risk of exposure of sensitive information.
    repository:
      owner: gitlab-com/support/toolbox
      name: winget-pkgs
      branch: winget-releases/{{ .Version }}
      token: "{{ .Env.WINGET_PROJECT_TOKEN }}"
      pull_request:
        enabled: true
    commit_author:
      name: "{{ .Env.GITLAB_USER_NAME }}"
      email: "{{ .Env.GITLAB_USER_EMAIL }}"


changelog:
  sort: asc
  filters:
    exclude:
      - "^docs"
      - "^doc"
      - "^test"
      - "^Merge branch"
