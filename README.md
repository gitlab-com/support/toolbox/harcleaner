# harcleaner

Command line tool to clean HAR files to reduce risk of exposure of sensitive information.

[[_TOC_]]

## Why?
HAR files can contain sensitive information. The information contained in HAR files can be used to
impersonate users and compromise accounts.

If GitLab Support has asked you to provide a HAR file for troubleshooting/investigating a problem
, this tool will help reduce the risk of sharing these files.

## Contributing

Check out the [contribution docs](CONTRIBUTING.md) for more information on contributing to this project.

## Features

harcleaner helps protect sensitive data in HAR files by:
- Processing every string field to find and redact sensitive data
- Removing potentially sensitive structures like cookies and POST data by default
- Supporting exceptions via configuration when certain data needs to be preserved

For detailed information about redaction behavior and configuration options, see [How Redaction Works](docs/redaction.md).


## Limitations

This tool helps reduce the risk of exposing sensitive data in HAR files, but cannot guarantee a complete removal of all sensitive information. Users should:
- Review HAR files manually before sharing
- Be aware that some sensitive data might not be detected
- Treat cleaned HAR files with appropriate caution

The tool is an aid in protecting sensitive data, not a replacement for careful handling of HAR files.


## :warning: Generating HAR files :warning:

To minimize sensitive data in your HAR files, use a new Private/Incognito browser window and limit the services you log into. See our [guide on generating HAR files](docs/generating-har-files.md) for recommendations.

## Installation

### Quick Start
Download the latest release from our [Releases page](https://gitlab.com/gitlab-com/support/toolbox/harcleaner/-/releases) for your platform, or install via Homebrew:

```bash
brew install gitlab-support/harcleaner/harcleaner
```

## Installation Options

### Pre-built binaries
Download the appropriate binary for your platform from our [Releases page](https://gitlab.com/gitlab-com/support/toolbox/harcleaner/-/releases):

- Linux (amd64)
- Windows (amd64)
- macOS (Intel and Apple Silicon)

### Homebrew

```bash
# Add the GitLab Support tap
brew tap gitlab-support/harcleaner https://gitlab.com/gitlab-com/support/toolbox/homebrew-harcleaner.git

# Install harcleaner
brew install harcleaner
```

### Docker
```bash
# Pull the latest release
docker pull registry.gitlab.com/gitlab-com/support/toolbox/harcleaner:latest

# Or a specific version
docker pull registry.gitlab.com/gitlab-com/support/toolbox/harcleaner:v2.1.0-amd64
```

See [Docker Usage](./docs/Docker.md) for running instructions.

### Build from source
```
# Clone repository
git clone https://gitlab.com/gitlab-com/support/toolbox/harcleaner.git
cd harcleaner

# Build binaries
make build
```

See our [contribution docs](CONTRIBUTING.md) for more details build instructions.


## Usage

Basic usage of harcleaner:

```bash
# Linux/macOS
harcleaner -input input.har -output clean.har

# Windows
harcleaner.exe -input input.har -output clean.har
```

### Command Line Options

```bash
-input string      Path to input file
-output string     Path to output file (default "clean.har")
-debug            Enable debug logging
-config-file      Path to config file
```

### Docker

harcleaner can be run in a Docker container. See [Docker Usage](./docs/docker.md) for detailed instructions.

## Configuration

More details can be found in the documentation for [Configuration](./docs/configuration.md).


## Providing HAR files to GitLab Support

Follow the directions of the Support Engineer that requested the HAR file.

## Troubleshooting

See our [Troubleshooting Guide](docs/troubleshooting.md) for common issues and solutions.